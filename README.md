# this is the course from NeHe_OpenGL_Qt4   
I transform it to the platform of vs2015 and Qt5.9   
and it still works very well   

# the course order:

1. CreatingAnOpenGLWindow
2. CreatingYourFirstPolygonAndQuad
3. FlatandSmoothColors
4. RotatingAPolygon
5. SolidObjects
6. TextureMapping
7. TextureFiltersAndBasicLighting
8. Blending
9. CreatingMovingScenesWithBlendedTextures
10. LoadingAndMovingThroughA3DWorld
11. WavingTextureMap
12. DisplayLists
13. CoolLookingFog
14. 2DBitmapTextureFont
15. Quadratics
16. TriangleStripAndParticleEngine
17. Masking
18. Amidar
19. BumpMapping
20. SphereMapping
21. Extensions
22. MorphingPoints
23. StencilAndReflection
24. ShadowCasting
25. BezierSurfaces
26. Blitter
27. CollisionDetection
28. PickingGame
29. HeightMapping
30. PlayAnAVI
31. RenderingToATexture
32. CelShading
33. LoadingBitmapsFromTheResource
34. SimplePhysicalSimulationEngine
35. RopePhysics
36. VolumetricFog
37. MultipleViewports
38. LensFlare
39. VertexBuffer
40. FSAA
42. ArcBallRotation
