#pragma once

#include <QGLWidget>
#include <QKeyEvent>
#define GL_PI 3.1415926
class OpenGL : public QGLWidget
{
public:
	OpenGL(QWidget *parent = Q_NULLPTR);
	~OpenGL();
protected:
	void initializeGL();
	void resizeGL(int width,int height);
	void paintGL();

	void timerEvent(QTimerEvent *event);
	void keyPressEvent(QKeyEvent *event);
	void loadGLTexture();

private:
	GLfloat anglex;
	GLfloat angley;
	bool m_light;
	bool m_blending;
	GLuint m_texture[1];

	GLfloat m_light_ambient[4];
	GLfloat m_light_diffuse[4];
	GLfloat m_light_position[4];
};
