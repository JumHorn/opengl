#include "OpenGL.h"
#include <GL/glu.h>

OpenGL::OpenGL(QWidget *parent)
	: QGLWidget(parent), anglex(0), angley(0),
	m_light_ambient { 0.5f, 0.5f, 0.5f, 1.0f },
	m_light_diffuse{ 1.0f, 1.0f, 1.0f, 1.0f },
	m_light_position{ 0.0f, 0.0f, 2.0f, 1.0f },
	m_light(true), m_blending(true)
{
	startTimer(200);
}

OpenGL::~OpenGL()
{
	glDeleteTextures(1, &m_texture[0]);
}

void OpenGL::initializeGL()
{
	//这样设置混合好看些
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);

	glLightfv(GL_LIGHT1, GL_AMBIENT, m_light_ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, m_light_diffuse);
	glLightfv(GL_LIGHT1, GL_POSITION, m_light_position);
	glEnable(GL_LIGHT1);
}

void OpenGL::resizeGL(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0,0,width,height);

	glMatrixMode(GL_PROJECTION);//投影矩阵
	glLoadIdentity();
	glOrtho(0, width, 0, height, 1.0f, -1.0f);

	glMatrixMode(GL_MODELVIEW);//观察矩阵
	glLoadIdentity();
}

void OpenGL::paintGL()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	//画闪电
	float x = 100, y = 100;
	glPushMatrix();
	glTranslatef(width() / 4, height() / 4*3, 0.0f);
	glColor3f(1.0f,0.0f,0.0f);
	glBegin(GL_TRIANGLES);
	glVertex2f(0.0f, 0.0f);
	glVertex2f(x / 2 * 1.3, y / 2 * 1.1);
	glVertex2f(x / 2 * 1.2, y / 2 * 1.1);


	glVertex2f(x, y);
	glVertex2f(x / 2 * 0.7, y / 2 * 0.9);
	glVertex2f(x / 2 * 0.8, y / 2 * 0.9);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex2f(x / 2 * 0.7, y / 2 * 0.9);
	glVertex2f(x / 2 * 0.8, y / 2 * 0.9);
	glVertex2f(x / 2 * 1.2, y / 2 * 1.1);
	glVertex2f(x / 2 * 1.3, y / 2 * 1.1);
	glEnd();

	glPopMatrix();

	//画飞机
	glPushMatrix();
	glTranslatef(width() / 4, height() / 2, 0.0f);
	glBegin(GL_LINE_LOOP);
	glVertex2f(0.0, 15.0);
	glVertex2f(4.0, 4.0);
	glVertex2f(4.0, 2.0);
	glVertex2f(12.0, 2.0);
	glVertex2f(15.0, -2.0);
	glVertex2f(3.0, -2.0);
	glVertex2f(3.0, -15.0);
	glVertex2f(6.0, -18.0);
	glVertex2f(-6.0, -18.0);
	glVertex2f(-3.0, -15.0);
	glVertex2f(-3.0, -2.0);
	glVertex2f(-15.0, -2.0);
	glVertex2f(-12.0, 2.0);
	glVertex2f(-4.0, 2.0);
	glVertex2f(-4.0, 4.0);
	glEnd();
	glPopMatrix();

	//画船
	glPushMatrix();
	glTranslatef(width() / 4, height() / 4, 0.0f);
	glBegin(GL_LINE_LOOP);
	glVertex2f(0.0, 30.0);
	glVertex2f(10.0,5.0);
	glVertex2f(7.0, -20.0);
	glVertex2f(-7.0, -20.0);
	glVertex2f(-10.0, 5.0);
	glEnd();
	glPopMatrix();

	//画波束
	glTranslatef(width() / 2, height() / 2, 0.0f);
	float arcs = 500;
	float angle = 3 / 360.0 * 2 * GL_PI;

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	for (float k = -angle; k <= angle+0.015; k += 0.01f)
	{
		float dx=arcs*cos(k), dy= arcs*sin(k);
		glVertex3f(dx, dy, 0);
	}
	glEnd();


	static int even = 0;
	int odd = 1;
	glBegin(GL_QUADS);
	even -= 8;
	if (even == -40)
		even = 0;
	for (float i = 0; i < 500;)
	{
		if (++odd % 2 == 1)
		{
			i += 10;
			continue;
		}
		int tmp = i + even;
		if (tmp < 0)
		{
			i += 30;
			continue;
		}
		for (float k = -angle; k < angle; k += 0.005f)
		{
			float dy0 = tmp*sin(k), dy1 = tmp*sin(k + 0.01), dy2 = (tmp + 10)*sin(k + 0.01), dy3 = (tmp + 10)*sin(k);
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f(tmp*cos(k) - (angle-abs(k))*100, dy0, 0);
			glVertex3f(tmp*cos(k + 0.01) - (angle - abs(k)) * 100, dy1, 0);
			glVertex3f((tmp+10)*cos(k+0.01) - (angle - abs(k)) * 100, dy2, 0);
			glVertex3f((tmp+10)*cos(k) - (angle - abs(k)) * 100, dy3, 0);
		}
		i += 30;
	}
	glEnd();
}

void OpenGL::timerEvent(QTimerEvent* event)
{
	anglex += 0.5;
	angley += 0.5;
	updateGL();
	QGLWidget::timerEvent(event);
}

void OpenGL::keyPressEvent(QKeyEvent *event)
{
	switch (event->key())
	{
	case Qt::Key_L:
		if (m_light)
		{
			glDisable(GL_LIGHTING);		// 禁用光源
		}
		else
		{
			glEnable(GL_LIGHTING);		// 启用光源
		}
		m_light = !m_light;
	case Qt::Key_B:
		if (m_blending)
		{
			glEnable(GL_BLEND);		// 打开混合
			glDisable(GL_DEPTH_TEST);	// 关闭深度测试
		}
		else
		{
			glDisable(GL_BLEND);		// 关闭混合
			glEnable(GL_DEPTH_TEST);	// 打开深度测试
		}
		m_blending = !m_blending;
		break;
	default:
		break;
	}
}

void OpenGL::loadGLTexture()
{
	//现在载入图像，并将其转换为纹理。
	QImage image(":/Resources/Koala.jpg");
	image = image.convertToFormat(QImage::Format_RGB888);
	image = image.mirrored();
	glGenTextures(1, &m_texture[0]);
	glBindTexture(GL_TEXTURE_2D, m_texture[0]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,
		image.width(), image.height(), 0, GL_RGB, GL_UNSIGNED_BYTE,
		image.bits());
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}